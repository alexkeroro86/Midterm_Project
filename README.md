# Software Studio 2018 Spring Midterm Project

[website link](https://software-studio-mid.firebaseapp.com/)

## Topic
* Forum
    * navbar
        * sign in/up
        * user/profile page and log out
    * category
        * subject
            * post
* Key functions
    1. user page shows posts one released or posts one commented
    2. post page under each subject
    3. post list page shows all pages depend on its subject
    4. leave comment under any post with the latest at the bottom
* Other functions
    1. text editor in post page
    2. post with image file or url supported
    3. posts in user page are sorted by subject then by last updated time
    4. user can change his name or email address
    5. 7 subjects in the main page
    6. client-side router such that each page can be bookmarked
    7. ability to handle wrong url by redirecting to not found page
    8. each user can only like one post once
    9. go top and go back button fix at the bottom-right corner
    10. display the number of likes and comments of post in the subject

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

#### 1. Authentication
* 註冊
    * 用信箱和密碼做註冊，還有一個是名字為必填，也可以當作個人簡介寫
    * 返回鈕是直接回到category的地方，浮動的返回箭頭是回到上個地方
* 登入
    * 第一種是用非第三方的登入依照信箱對密碼，如果輸入錯誤會跳致中的訊息方塊
    * 另一種以Google登入，並且預設使用者姓名和Google email時用的一樣
    * 登入完後都會重新導向category，如果要確認是否登入可按右上角人頭

#### 2. RWD
* category和subject的預覽圖都是用bootstrap的card做的，其中針對螢幕寬度做三到一欄的變化
* card的效果可以依據圖片和文字做不等的排版，看起來比較動態
* 進板圖則是將高度依視窗寬度變化，在css方面用object-fit:cover來讓圖片在不失真的情況下填滿
* post中的作者名和貼文內容則會從原本的左右8比3，變成上下排版
* navbar的內容則會全部至中表現不會偏，若在螢幕比較大時下拉的選單是向左延伸，這樣即便選單在左邊也不會影響到內容

#### 3. Client-side routing
* 主要目的是讓這篇文章可以被直接加入書籤，方便下次直接以這個網址連結
* 用到的行為和#在網頁中的內容可以直接跳到那地方，而用#!可以更改連結和以RegExp物件對網址做解讀，不會產生網頁明顯跳轉的感覺
* 以on幫這個型別的網址做註冊，用navigate對網址做fire，而getFragment就對網址傳的內容作擷取，最後加listen定時確認現在的網址以便動作
* 參考這篇教學: [A modern JavaScript router in 100 lines](http://krasimirtsonev.com/blog/article/A-modern-JavaScript-router-in-100-lines-history-api-pushState-hash-url)

#### 4. Text editor
* 可以根據檔案上傳或圖片連結上傳封面圖，但只能二擇一
* 有四種編輯可以操作，分別為粗體、斜體、顏色和超連結，用反白的方式比較好用，主要用到tag的contenteditable和document的execCommand

#### 5. Update
* 可以更改個人檔案，因為user底下有存post的id和subject，所以當更新成功時，還會到有出現過的貼文將原本的名字改成新的
* 登入時的信箱也可以更改，下次登入就可以用新的

#### 6. Not found
* 當網址進入不存在的板塊或不存在的連結會跳到not found的頁面，而且只能選擇跳回主頁，原本右下角的回鍵在這邊會消失
* 根據有沒有成功找到post在database或是不是存在的板塊

#### 7. Component based
* 根據每個功能將js和css分開寫，方便找問題是出在哪個功能的function上

#### 8. 進板圖
* 每個主題都有自己的圖片，而且圖片會根據現在大小作調整，並非單純填滿導致圖片變形，
* 用vh為單位而不是wh的好處在會比較好看，螢幕高度的變化比較小且大小適中

#### 9. 動畫和CSS
* 所有頁面載入都有淡出的效果，因為css無法對display由none轉block有transition延遲的效果，所以必須先display:block後，再從原本的透明度由0轉到1，才可以搭配到css的transition延遲
* 貼文的動畫參考部落格和[CSS-Tricks](https://css-tricks.com/)加以修改
* 每個card都會隨滑鼠作相對應特效
* 個人網站的標題、按讚和下個的小工具當滑鼠移上去都會放大
* Not Found頁面字會彈跳一下
* 有特製的scroll-bar和navbar的字體用monospace會整齊一點

#### 10. 小工具
* 至頂鍵只有在頁面不在最頂端時出現
* 返回鍵直接在旁邊按就可以返回
* 點標題會直接回到主頁面
* 在點擊新增留言時，頁面會移到最底端方便打字

#### 11. 個人網站
* 根據主題和最後更新時間排序呈現
* 點標題可以連到那個文章

#### 12. 解決延遲
* 如果在讀貼文串時，還一併讀取圖片，會讓那則文章變到比較後面才插入，所以必須先只讀文章的字，且將包住文章的div分配到那則為一的id，然後再到storage去讀圖片，因為post指對應到一張圖片，所以圖片存到storage的名字就用那則貼文的id，等讀完後在選到那個id的div更新img tag，最後就可以按照真正按照時間排序
* 在user page也是利用圖樣的方式，再加上因為要對各板的某一文章讀取，所以板圖必須再讀取user的posts下時先放好，這樣在一時間讀文章時根據那個文章插入在對應的板下

#### 13. 按讚一個帳戶只能用一次
* 根據database的資料格式
    * ![m_user](readme-img/m_user.png)
    * ![m_list](readme-img/m_list.png)
    * ![m_imageUrl](readme-img/m_imageUrl.png)
* 當按讚的時候會將user的uid加到那則文章下的like下，所以下次在按時會先確定底下有沒有這個人的uid後才決定這個動作是否有效

#### 14. 資料儲存格式
* 把user和post的key和uid設成一樣的，這樣如果要從user底下用的postId直接到m_list底下在查詢比較方便
* 每個comment和post都要存作者的uid，這樣的話在作者更新個人資料時，對其他地方更新就一不會變的uid來找比較快
* 圖片的連結另外存是對非會員做處理，而以那篇文章的id作為key，方便之後載入時可以另外非同步更新

#### 15. 通知
* 只有網頁在開著的時時才有效，針對有新的文章跳出，如果點擊可以快速連結到那個文章

## Security Report (Optional)

#### 1. Realtime database
* 因為規則不是過濾器，所以將post中image的url分別存取，而本身的post只存封面圖的值是url或true、false代表是連結、檔案或沒有，這麼做就可以把不是會員的人看不到文章的圖片即使是連結
* 若不是會員能操作的只能讀，不能做任何寫的功能，但是NOTICES板只有會員可以讀寫

#### 2. Storage
* 可以讀寫的人只有會員而已

## PS

#### iLMS的zip檔裡放的README.md是舊的，以gitlab上的為最新的