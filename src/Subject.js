import Component from  './Component.js';
import Editor from './Editor.js';

import './Subject.css';

export default class Subject extends Component {
    static getRootClass() {
        return '.m_subject';
    }

    constructor(root, subjectName) {
        super(root);

        setTimeout(() => {
            root.style.opacity = "1";
        }, 10);

        this.m_editor = new Editor(root.querySelector(Editor.getRootClass()));
        this.subjectName = subjectName;

        let img = root.querySelector('#m_subjectWelcome img');
        img.src = `./images/${subjectName}.jpg`;
        let title = root.querySelector('#m_subjectWelcome h1');
        title.textContent = subjectName.toUpperCase();

        let text = "New Post!";
        for(let i in text) { 
            let node = document.createElement('span');
            if(text[i] === " ") {
                node.innerHTML = '&nbsp;';
            }
            else {  
                node.innerHTML = `${text[i]}`;
            }
            root.querySelector('.wavetext').appendChild(node);
        }

        this.root.querySelector('#upload-img').addEventListener('change', evt => {
            if(evt.target.files && evt.target.files[0]) {
                var fr = new FileReader();
                fr.onload = (e) => {
                    document.querySelector('#m_uploadPic').src = e.target.result;
                };
                fr.readAsDataURL(evt.target.files[0]);
            }
            else {
                alert(`Failed to upload image!`);
            }
        });
        this.root.querySelector('#upload-img-url').addEventListener('change', evt => {
            if(evt.target.value != '') {
                console.log(evt.target.value);
                document.querySelector('#m_uploadPic').src = evt.target.value;
                this.root.querySelector('#upload-img-wrapper').style.display = 'none';
            }
            else {
                document.querySelector('#m_uploadPic').src = '';
                this.root.querySelector('#upload-img-wrapper').style.display = 'inline-block';
            }
        });

        this.m_post = this.root.querySelector('.m_post');
        this.m_newPost = root.querySelector('.m_newPost').addEventListener('click', () => {
            this.m_post.style.display = (this.m_post.style.display === 'block') ? 'none' : 'block';
        });

        this.listRef = firebase.database().ref(`m_list/${this.subjectName}`);
        this.root.querySelector('.m_postBtn').addEventListener('click', () => {
            let m_title = this.root.querySelector('.m_title textarea');
            let m_comment = this.root.querySelector('.m_comment .m_editor');
            console.log(m_title.value, m_comment.innerHTML);
            let user = firebase.auth().currentUser;
            if(user) {
                firebase.database().ref(`m_user/${user.uid}`).once('value').then(userProfile => {
                    if(m_title.value !== '' && m_comment.innerHTML !== '') {
                        var pushRef = this.listRef.push();
                        var now = new Date();
                        var parseDate = `${now.getFullYear()}/${now.getMonth() + 1}/${now.getDate()} ${now.getHours()}:${(now.getMinutes() < 10 ? '0':'') + now.getMinutes()}`;
                        firebase.database().ref(`m_list/${this.subjectName}/${pushRef.key}`).set({
                            authorUid: userProfile.val().uid,
                            authorName: userProfile.val().name,
                            postId: pushRef.key,
                            postCoverPic: `${document.querySelector('#upload-img-url').value != '' ? 'url' : document.querySelector('#m_uploadPic').src ? 'true' : 'false'}`,
                            postTitle: m_title.value,
                            postContent: m_comment.innerHTML,
                            postDate: parseDate
                        });
                        if(document.querySelector('#upload-img-url').value != '') {
                            firebase.database().ref(`m_imageUrl/${this.subjectName}/${pushRef.key}`).set({
                                postCoverPicUrl: document.querySelector('#upload-img-url').value
                            });
                        }
                        else if(document.querySelector('#m_uploadPic').src) {
                            firebase.storage().ref(`images/${pushRef.key}`).putString(document.querySelector('#m_uploadPic').src, 'data_url').then(snapshot => {
                                console.log(`Upload cover picture success`);
                            }).catch(err => { console.log(err.message); });
                        }
                        firebase.database().ref(`m_user/${userProfile.val().uid}/posts/${this.subjectName}/${pushRef.key}`).set({
                            postId: pushRef.key,
                            postTitle: m_title.value,
                            postDate: parseDate
                        });
                        this.m_post.style.display = 'none';
                        m_title.value = '';
                        m_comment.innerHTML = '';
                        document.querySelector('#m_uploadPic').src = '';
                    }
                }).catch(err => { console.log(err.message); });
            }
            else {
                alert(`Please log in!`);
            }
        });

        this.m_subjectList = root.querySelector('#m_subjectList');
        this.listRef.once('value').then(snapshot => {
            console.log(snapshot.key);
            if((snapshot.key != 'games' && snapshot.key !=  'animals' && snapshot.key != 'notices' &&
                snapshot.key != 'studies'&& snapshot.key != 'sports' && snapshot.key != 'foods' && snapshot.key != 'news')
                && snapshot.numChildren() == 0) {
                this.fire('subjectNotFound');
            }

            var first_count = 0;
            var second_count = 0;

            snapshot.forEach(childSnapshot => {
                let childData = childSnapshot.val();
                let node = document.createElement('div');
                node.classList.add('my-3', 'rounded', 'white', 'form-control', 'm_subjectPost', 'card');
                node.id = `${childData.postId}`;
                node.innerHTML = `
                <img class="card-img-top">
                <div class="card-body">
                    <div class="m_postTitle card-title">
                        ${childData.postTitle}
                    </div>
                    <div class="m_postTitle card-title">
                        <span><i class="far fa-thumbs-up"></i>${(childSnapshot.hasChild('like')) ? childSnapshot.child('like').numChildren() : 0}</span>
                        <span>&nbsp;&nbsp;<i class="far fa-comment"></i>${childSnapshot.hasChild('m_comments') ? childSnapshot.child('m_comments').numChildren() : 0}</span>
                    </div>
                    <div class="m_authorName card-text">
                        ${childData.authorName}
                    </div>
                    <div class="m_postDate card-text">
                        ${childData.postDate}
                    </div>
                </div>
                `;
                this.m_subjectList.insertBefore(node, this.m_subjectList.childNodes[0]);
                ++first_count;
                console.log(snapshot.numChildren(), first_count);

                node.addEventListener('click', () => {
                    this.fire('postClick', subjectName, childData.postId);
                });
                if(childData.postCoverPic === 'true') {
                    firebase.storage().ref(`images/${childData.postId}`).getDownloadURL().then(url => {
                        document.querySelector(`#${childData.postId} img`).src = url;
                        console.log(`foreach success`);
                    }).catch(err => {
                        document.querySelector(`#${childData.postId} img`).src = './images/MemberOnly.jpg';
                        console.log(err.message);}
                    );
                }
                else if(childData.postCoverPic === 'url') {
                    firebase.database().ref(`m_imageUrl/${this.subjectName}/${childData.postId}`).once('value').then(url => {
                        document.querySelector(`#${childData.postId} img`).src = url.val().postCoverPicUrl;
                    }).catch(err => {
                        document.querySelector(`#${childData.postId} img`).src = './images/MemberOnly.jpg';
                        console.log(err.message);
                    });
                }
            });
            this.listRef.on('child_added', childSnapshot => {
                ++second_count;
                console.log(first_count, second_count);
                if(second_count > first_count) {
                    let childData = childSnapshot.val();
                    let node = document.createElement('div');
                    node.classList.add('my-3', 'rounded', 'white', 'form-control', 'm_subjectPost', 'card');
                    node.id = `${childData.postId}`;
                    node.innerHTML = `
                    <img class="card-img-top">
                    <div class="card-body">
                        <div class="m_postTitle card-title">
                            ${childData.postTitle}
                        </div>
                        <div class="m_postTitle card-title">
                            <span><i class="far fa-thumbs-up"></i>${(childSnapshot.hasChild('like')) ? childSnapshot.child('like').numChildren() : 0}</span>
                            <span>&nbsp;&nbsp;<i class="far fa-comment"></i>${childSnapshot.hasChild('m_comments') ? childSnapshot.child('m_comments').numChildren() : 0}</span>
                        </div>
                        <div class="m_authorName card-text">
                            ${childData.authorName}
                        </div>
                        <div class="m_postDate card-text">
                            ${childData.postDate}
                        </div>
                    </div>
                    `;
                    this.m_subjectList.insertBefore(node, this.m_subjectList.childNodes[0]);
                    node.addEventListener('click', () => {
                        this.fire('postClick', subjectName, childData.postId);
                    });
                    
                    if(childData.postCoverPic === 'true') {
                        console.log(`Subject.js: 189 => child_added updates img`);
                        setTimeout(() => {
                            firebase.storage().ref(`images/${childData.postId}`).getDownloadURL().then(url => {
                                document.querySelector(`#${childData.postId} img`).src = url;
                                console.log(`child_added success`);
                            }).catch(err => { console.log(err.message);});
                        }, 1500);
                    }
                    else if(childData.postCoverPic === 'url') {
                        firebase.database().ref(`m_imageUrl/${this.subjectName}/${childData.postId}`).once('value').then(url => {
                            document.querySelector(`#${childData.postId} img`).src = url.val().postCoverPicUrl;
                        }).catch(err => {
                            document.querySelector(`#${childData.postId} img`).src = './images/MemberOnly.jpg';
                            console.log(err.message);
                        });
                    }

                    var notify = new Notification(`New Post from ${childData.authorName}` , {
                        body: `${childData.postTitle}`,
                        icon: './images/THF.ico'
                    });
                    
                    notify.onclick = function(e) {
                        e.preventDefault();
                        window.open(`https://software-studio-mid.firebaseapp.com/#!/category/${snapshot.key}/${childData.postId}`);
                    }
                }
            });
        }).catch(err => { console.log(err.message); });
    }

    reset() {
        // do nothing
    }
}