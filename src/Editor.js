import Component from  './Component.js';

import './Editor.css';

export default class Editor extends Component {
    static getRootClass() {
        return '.m_editor';
    }

    constructor(root) {
        super(root);

        this.m_editorBar = document.querySelector('.m_editorBar');

        this.m_toBold = document.querySelector('#m_toBold');
        this.m_toBold.addEventListener('click', this.toBold.bind(this));
        this.m_toItalic = document.querySelector('#m_toItalic');
        this.m_toItalic.addEventListener('click', this.toItalic.bind(this));
        this.m_toColor = document.querySelector('#m_toColor');
        this.m_toColor.addEventListener('change', this.changeFontColor.bind(this));
        this.m_toLink = document.querySelector('#m_toLink');
        this.m_toLink.addEventListener('click', this.toLink.bind(this));
    }

    toBold() {
        document.execCommand ('bold', false, null);
        console.log(this.root.innerHTML);
    }
    toItalic() {
        document.execCommand ('italic', false, null);
        console.log(this.root.innerHTML);
    }
    toColor() {
        document.execCommand('foreColor', false, this.m_toColor.value);
        console.log(this.root.innerHTML)
    }
    changeFontColor() {
        this.toColor();
    }
    toLink() {
        document.execCommand('createLink', false, window.getSelection());
        let aLinks = document.querySelectorAll('.m_editor a');
        for(let i = 0; i < aLinks.length; ++i) {
            aLinks[i].setAttribute('target', '_blank');
            aLinks[i].addEventListener('click', () => {
                // window.open(aLinks[i].href);
            });
        }
        console.log(this.root.innerHTML)
    }
}