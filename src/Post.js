import Component from  './Component.js';
import Editor from './Editor.js';

import './Post.css';

export default class Post extends Component {
    static getRootClass() {
        return '.m_postArticleWrapper';
    }

    constructor(root, subjectName, postId) {
        super(root);

        setTimeout(() => {
            root.style.opacity = "1";
        }, 10);

        this.subjectName = subjectName;
        this.postId = postId;
        this.m_postComment = root.querySelector('#m_postComment');
        this.postLike = '0';
        console.log(`${this.postLike}, ${Number(this.postLike)}`)

        let img = root.querySelector('#m_subjectWelcome img');
        img.src = `./images/${subjectName}.jpg`;
        let title = root.querySelector('#m_subjectWelcome h1');
        title.textContent = subjectName.toUpperCase();

        let text = "New Comment!";
        for(let i in text) { 
            let node = document.createElement('span');
            if(text[i] === " ") {
                node.innerHTML = '&nbsp;';
            }
            else {  
                node.innerHTML = `${text[i]}`;
            }
            root.querySelector('.wavetext').appendChild(node);
        }
        this.m_postArticleCommentWrapper = this.root.querySelector('.m_postArticleCommentWrapper');
        this.m_newComment = root.querySelector('.m_newComment').addEventListener('click', () => {
            this.m_postArticleCommentWrapper.style.display = (this.m_postArticleCommentWrapper.style.display === 'block') ? 'none' : 'block';
            document.body.scrollTop =  document.body.scrollHeight;
            document.documentElement.scrollTop =  document.documentElement.scrollHeight;
        });

        firebase.database().ref(`m_list/${this.subjectName}`).once('value').then(test => {
            if(test.hasChild(`${postId}`)) {
                firebase.database().ref(`m_list/${this.subjectName}/${postId}`).once('value').then(snapshot => {
                    let postVal = snapshot.val();
                    console.log(postVal);
                    root.querySelector('.m_postArticleTitle').textContent = postVal.postTitle;
                    root.querySelector('.m_postArticleAuthor').textContent = postVal.authorName;
                    root.querySelector('.m_postArticleDate').textContent = postVal.postDate;
                    root.querySelector('.m_postArticlePost').innerHTML = postVal.postContent;
                    this.postLike = (snapshot.hasChild('like')) ? snapshot.child('like').numChildren() : '0';
                    console.log(this.postLike);
                    root.querySelector('.m_postArticleLikeNum').textContent = this.postLike;
                    
                    if(postVal.postCoverPic === 'true') {
                        firebase.storage().ref(`images/${postVal.postId}`).getDownloadURL().then(url => {
                            document.querySelector(`.m_postArticleCover`).src = url;
                            console.log(`foreach success`);
                        }).catch(err => {
                            document.querySelector(`.m_postArticleCover`).src = './images/MemberOnly.jpg';
                            console.log(err.message);
                        });
                    }
                    else if(postVal.postCoverPic === 'url') {
                        firebase.database().ref(`m_imageUrl/${this.subjectName}/${postVal.postId}`).once('value').then(url => {
                            document.querySelector(`.m_postArticleCover`).src = url.val().postCoverPicUrl;
                        }).catch(err => {
                            document.querySelector(`.m_postArticleCover`).src = './images/MemberOnly.jpg';
                            console.log(err.message);
                        });
                    }
                    else {
                        document.querySelector('.m_postArticleCover').remove();
                    }
                }).catch(err => { console.log(err.message); });
            }
            else {
                this.fire('postNotFound');
            }
        }).catch(err => { console.log(err.message); });

        this.root.querySelector('.m_postArticleLike').addEventListener('click', () => {
            firebase.database().ref(`m_list/${this.subjectName}/${postId}`).once('value').then(snapshot => {
                let user = firebase.auth().currentUser;
                if(snapshot.hasChild('like')) {
                    let userLiked = false;
                    snapshot.child('like').forEach(childSnapshot => {
                        console.log(childSnapshot.val().likeUserUid);
                        if(childSnapshot.val().likeUserUid == user.uid) {
                            userLiked = true;
                        }
                    });
                    if(!userLiked) {
                        firebase.database().ref(`m_list/${this.subjectName}/${postId}/like/${user.uid}`).set({
                            likeUserUid: user.uid
                        });
                    }
                }
                else {
                    firebase.database().ref(`m_list/${this.subjectName}/${postId}/like/${user.uid}`).set({
                        likeUserUid: user.uid
                    });
                }
            });
        });
        firebase.database().ref(`m_list/${this.subjectName}/${postId}/like`).on('value', snapshot => {
            this.postLike = snapshot.numChildren();
            root.querySelector('.m_postArticleLikeNum').textContent = this.postLike;
        });

        this.commentRef = firebase.database().ref(`m_list/${this.subjectName}/${postId}/m_comments`);

        this.root.querySelector('.m_commentBtn').addEventListener('click', () => {
            let m_comment = this.root.querySelector('.m_postArticleCommentWrapper textarea');
            let user = firebase.auth().currentUser;
            if(user) {
                firebase.database().ref(`m_user/${user.uid}`).once('value').then(userProfile => {
                    let pushRef = this.commentRef.push();
                    let now = new Date();
                    let parseNow = `${now.getFullYear()}/${now.getMonth() + 1}/${now.getDate()} ${now.getHours()}:${(now.getMinutes() < 10 ? '0':'') + now.getMinutes()}`;
                    firebase.database().ref(`m_list/${this.subjectName}/${postId}/m_comments/${pushRef.key}`).set({
                        authorUid: userProfile.val().uid,
                        authorName: userProfile.val().name,
                        commentId: pushRef.key,
                        commentContent: m_comment.value,
                        commentDate: parseNow
                    });
                    firebase.database().ref(`m_list/${this.subjectName}/${postId}`).update({
                        postDate: parseNow
                    });
                    firebase.database().ref(`m_user/${userProfile.val().uid}/posts/${this.subjectName}/${postId}`).set({
                        postId: postId,
                        postTitle: root.querySelector('.m_postArticleTitle').textContent,
                        postDate: parseNow
                    });
                    m_comment.value = '';
                }).catch(err => { console.log(err.message); });
            }
            else {
                alert(`Please log in!`);
            }
        });
        this.commentRef.orderByChild('commentDate').once('value').then(snapshot => {
            var first_count = 0;
            var second_count = 0;

            snapshot.forEach(childSnapshot => {
                let childData = childSnapshot.val();
                let node = document.createElement('div');
                node.classList.add('my-3', 'rounded', 'white', 'form-control', 'm_container');
                node.id = `${childData.commentId}`;
                node.innerHTML = `
                <div id="m_postCommentName">
                    ${childData.authorName}
                </div>
                <div id="m_postCommentContent">  
                    ${childData.commentContent}
                </div>
                <div id="m_postCommentDate"> 
                    ${childData.commentDate}
                </div>
                `;
                this.m_postComment.appendChild(node);
                ++first_count;
            });
            this.commentRef.on('child_added', childSnapshot => {
                ++second_count;
                if(second_count > first_count) {
                    let childData = childSnapshot.val();
                    let node = document.createElement('div');
                    node.classList.add('my-3', 'rounded', 'white', 'form-control', 'm_container');
                    node.id = `${childData.commentId}`;
                    node.innerHTML = `
                    <div id="m_postCommentName">
                        ${childData.authorName}
                    </div>
                    <div id="m_postCommentContent">  
                        ${childData.commentContent}
                    </div>
                    <div id="m_postCommentDate">  
                        ${childData.commentDate}
                    </div>
                    `;
                    this.m_postComment.appendChild(node);
                }
            });
        }).catch(err => { console.log(err.message); });
    }
    
    reset() {
        // do nothing
    }
}