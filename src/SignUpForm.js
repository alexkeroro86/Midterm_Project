import Component from  './Component.js';

import './SignUpForm.css';

export default class SignUpForm extends Component {
    static getRootClass() {
        return '.m_signUpForm';
    }

    constructor(root) {
        super(root);

        this.m_alert = document.querySelector('.m_alert');

        setTimeout(() => {
            root.style.opacity = "1";
        }, 10);

        this.inputEmail = root.querySelector('#inputEmail');
        this.inputPassword = root.querySelector('#inputPassword');
        this.inputUsername = root.querySelector('#inputUsername');

        this.btnSignUp = root.querySelector('#btnSignUp');
        this.btnSignUpBack = root.querySelector('#btnSignUpBack');

        this.btnSignUp.addEventListener('click', () => {
            let email = inputEmail.value;
            let password = inputPassword.value;
            let name = inputUsername.value;

            if(name !== '') {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then(user => {
                        this.create_alert("success", "Welcome to TsingHua Forum");
                        firebase.database().ref(`m_user/${user.uid}`).set({
                            uid: user.uid,
                            email: email,
                            password: password,
                            name: name
                        });
                        setTimeout(() => {
                            this.fire('signUpSuccess');
                        }, 1000);
                    })
                    .catch(err => {
                        this.create_alert("error", err.message);
                        inputEmail.value = "";
                        inputPassword.value = "";
                    });
            }
            else {
                this.create_alert("error", "Username can not be skipped!");
            }
        });
        this.btnSignUpBack.addEventListener('click', () => {
            this.fire('signUpBack');
        });
    }

    reset() {
        // do nothing
    }

    create_alert(type, message) {
        if (type == "success") {
            this.m_alert.innerHTML = `
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
                <strong>Success!</strong>
                <span>${message}</span>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            </div>
            `;
        } else if (type == "error") {
            this.m_alert.innerHTML = `
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                <strong>Error!</strong>
                <span>${message}</span>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            </div>
            `;
        }
    }
}