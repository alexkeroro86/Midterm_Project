import Component from  './Component.js';

import './ProfileForm.css';

export default class ProfileForm extends Component {
    static getRootClass() {
        return '.m_profileForm';
    }

    constructor(root, userProfile) {
        super(root);

        setTimeout(() => {
            root.style.opacity = "1";
        }, 10);

        this.preName = userProfile.name;

        this.m_alert = document.querySelector('.m_alert');

        this.inputName = root.querySelector('#inputName');
        this.inputEmail = root.querySelector('#inputEmail');

        this.btnUpdate = root.querySelector('#btnUpdate');
        this.btnProfileBack = root.querySelector('#btnProfileBack');

        this.btnUpdate.addEventListener('click', () => {
            let name = this.inputName.value;
            let email = this.inputEmail.value;

            let user = firebase.auth().currentUser;

            if(name !== userProfile.name) {
                firebase.database().ref(`m_user/${userProfile.uid}`).update({
                    name: name
                }).then(() => {
                    this.create_alert('success', 'user name updated!');
                    firebase.database().ref(`m_user/${userProfile.uid}`).once('value').then(snapshot => {
                        if(snapshot.hasChild('posts')) {
                            snapshot.child('posts').forEach(childSnapshot => {
                                firebase.database().ref(`m_user/${userProfile.uid}/posts/${childSnapshot.key}`).once('value').then(userPosts => {
                                    userPosts.forEach(userPost => {
                                        // console.log(userPost.val());
                                        // console.log(`Updated post: ${userPost.val().authorName}`);
                                        // if(userPost.val().authorName == this.preName) {
                                        //     firebase.database().ref(`m_list/${childSnapshot.key}/${userPost.val().postId}`).update({
                                        //         authorName: name
                                        //     });
                                        // }
                                        firebase.database().ref(`m_list/${childSnapshot.key}/${userPost.val().postId}`).once('value').then(getUserPost => {
                                            console.log(getUserPost.val());
                                            if(getUserPost.val().authorName == this.preName) {
                                                getUserPost.getRef().update({
                                                    authorName: name
                                                });
                                            }
                                            // getUserPost.forEach(chlidGetUserPost => {
                                            //     if(chlidGetUserPost.val().authorName == this.preName) {
                                            //         chlidGetUserPost.getRef().update({
                                            //             authorName: name
                                            //         });
                                            //     }
                                            // });
                                        }).catch(err => { alert(err.message); });
                                        // console.log(`Update comment: m_list/${childSnapshot.key}/${userPost.val().postId}/m_comments}`);
                                        firebase.database().ref(`m_list/${childSnapshot.key}/${userPost.val().postId}/m_comments`).orderByChild('authorName').once('value').then(getComment => {
                                            // firebase.database().ref(`m_list/${childSnapshot.key}/${userPost.val().postId}/m_comments/${getComment.getRef().key}`).update({
                                            //     authorName: name
                                            // });
                                            getComment.forEach(childGetComment => {
                                                if(childGetComment.val().authorName == this.preName) {
                                                    childGetComment.getRef().update({
                                                        authorName: name
                                                    });
                                                }
                                            });
                                            // console.log(`Updated comment`, getComment.val());
                                        }).catch(err => { alert(err.message); });
                                    });
                                }).catch(err => { alert(err.message); });
                            });
                        }
                    });
                });
            }
            if(email !== userProfile.email) {
                let credential = firebase.auth.EmailAuthProvider.credential(
                    userProfile.email,
                    userProfile.password
                );
                user.reauthenticateWithCredential(credential).then(() => {
                    user.updateEmail(email).then(() => {
                        firebase.database().ref(`m_user/${userProfile.uid}`).update({
                            email: email
                        }).then(() => {this.create_alert('success', 'email updated!');});
                    }).catch(err => {
                        this.create_alert('error', err.message);
                    });
                }).catch(err => {
                    this.create_alert('error', err.message);
                });
            }
        });
        this.btnProfileBack.addEventListener('click', () => {
            this.fire('profileBack');
        });
    }

    reset() {
        // do nothing
    }

    create_alert(type, message) {
        if (type == "success") {
            this.m_alert.innerHTML = `
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
                <strong>Success!</strong>
                <span>${message}</span>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            </div>
            `;
        } else if (type == "error") {
            this.m_alert.innerHTML = `
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                <strong>Error!</strong>
                <span>${message}</span>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            </div>
            `;
        }
    }
}