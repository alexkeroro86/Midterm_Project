import Component from  './Component.js';

import './Navbar.css';

export default class Navbar extends Component {
    static getRootClass() {
        return '.m_navbar';
    }

    constructor(root) {
        super(root);
        this.root = root;

        this.m_accList = root.querySelector('.m_accList');

        firebase.auth().onAuthStateChanged(user => {
            if(user) {
                firebase.database().ref(`m_user/${user.uid}`).on('value',userProfile => {
                    this.m_accList.innerHTML = `
                    <button class="dropdown-item m_personalPageBtn" type="button">${userProfile.val().name}<br/>${user.email}</button>
                    <div class="dropdown-divider"></div>
                    <button class="dropdown-item m_profileBtn" type="button">Profile</button>
                    <div class="dropdown-divider"></div>
                    <button class="dropdown-item m_logOutBtn" type="button">Log out</button>
                    `;
    
                    this.root.querySelector('.m_logOutBtn').addEventListener('click', () => {
                        firebase.auth().signOut().then(res => {
                            this.fire('logOutBtnClick');
                        }).catch(err => { alert(err.message); });
                    });
                    this.root.querySelector('.m_profileBtn').addEventListener('click', () => {
                        this.fire('profileBtnClick', userProfile.val());
                    });
                    this.root.querySelector('.m_personalPageBtn').addEventListener('click', () => {
                        this.fire('personalPageBtnClick', userProfile.val());
                    });
                });
            }
            else {
                this.m_accList.innerHTML = `
                <button class="dropdown-item m_signInBtn" type="button">Sign in</button>
                <button class="dropdown-item m_signUpBtn" type="button">Sign up</button>
                `;

                this.root.querySelector('.m_signInBtn').addEventListener('click', () => {
                    this.fire('signInBtnClick');
                });
                this.root.querySelector('.m_signUpBtn').addEventListener('click', () => {
                    this.fire('signUpBtnClick');
                });
            }
        });
    }

    reset() {
        // do nothing
    }
}