import Component from  './Component.js';

import './Page.css';

export default class Page extends Component {
    static getRootClass() {
        return '.m_personalPage';
    }

    constructor(root, userProfileVal) {
        super(root);
        setTimeout(() => {
            root.style.opacity = "1";
        }, 10);

        this.reversePosts = [];
        this.hasSubjectList = [];
        this.subject = ['games', 'news', 'notices', 'sports', 'animals', 'studies', 'foods'];

        for(let name of this.subject) {
            console.log(name);
            let node = document.createElement('div');
            node.id = `${name}`;
            node.innerHTML = `
            <div id="m_subjectWelcome" class="row m_container my-4">
                <img class="img-fluid" width="880px" height="220px">
                <h1><h1>
            </div>
            `;
            this.root.appendChild(node);
            let img = root.querySelector(`#${name} img`);
            img.src = `./images/${name}.jpg`;
            let title = root.querySelector(`#${name} h1`);
            title.textContent = name.toUpperCase();
        }

        firebase.database().ref(`m_user/${userProfileVal.uid}`).once('value').then(snapshot => {
            // console.log(snapshot.val());
            if(snapshot.hasChild('posts')) {
                snapshot.child('posts').forEach(childSnapshot => {
                    // console.log(childSnapshot.key, childSnapshot.val());

                    this.hasSubjectList.push(childSnapshot.key);

                    firebase.database().ref(`m_user/${userProfileVal.uid}/posts/${childSnapshot.key}`).orderByChild('postDate').once('value').then(userPosts => {
                        userPosts.forEach(userPost => {
                            // console.log(userPost.val().postId);

                            this.reversePosts.push(userPost.val());
                            console.log(this.reversePosts.length, userPosts.numChildren());
                        });
                        for(let i = this.reversePosts.length - 1; i >= 0; --i) {
                            // console.log(i, this.reversePosts[i].postId, this.reversePosts.length);
                            firebase.database().ref(`m_list/${childSnapshot.key}/${this.reversePosts[i].postId}`).once('value').then(getPost => {
                                // console.log(getPost, getPost.key, childSnapshot.key);
                                // console.log(getPost.val().postId, this.reversePosts[i].postId);
                                console.log(getPost.val());
                                if(getPost.val() != null) {
                                    let postNode = document.createElement('div');
                                    // console.log(getPost.val().postId, this.reversePosts[i].postId);
                                    postNode.id = `${getPost.val().postId}`;
                                    postNode.innerHTML = `
                                    <div class="m_postArticle row mt-2 m_container form-control">
                                        <div class="row">
                                            <img class="m_postArticleCover">
                                            <div class="w-100"></div>
                                            <h2 class="m_postArticleTitle">${getPost.val().postTitle}</h2>
                                        </div>
                                        <div class="row m-3">
                                            <div class="m_postArticleAnD col-md-3 col-12">
                                                <span class="m_postArticleAuthor">${getPost.val().authorName}</span><br/>
                                                <span class="m_postArticleDate" style="color:#fcfcfc;font-size:1rem;position:absolute;bottom:0;right:0;">${getPost.val().postDate}</span> 
                                            </div>
                                            <div class="m_postArticlePost col-md-8 col-12">${getPost.val().postContent}</div>
                                            <div class="m_postArticleLikePage"><i class="far fa-thumbs-up"></i><span class="m_postArticleLikeNum">${(getPost.hasChild('like')) ? getPost.child('like').numChildren() : '0'}</span></div>
                                        </div>
                                    </div>
                                    `;
                                    // document.querySelector(`.m_subjectName${getPost.key}`).appendChild(postNode);
                                    document.querySelector(`#${childSnapshot.key}`).appendChild(postNode);
                                    // this.root.appendChild(postNode);

                                    this.root.querySelector(`#${getPost.val().postId} .m_postArticleTitle`).addEventListener('click', () => {
                                        this.fire('personalPagePostClick', childSnapshot.key, getPost.val().postId);
                                    });

                                    if(getPost.val().postCoverPic === 'true') {
                                        firebase.storage().ref(`images/${getPost.val().postId}`).getDownloadURL().then(url => {
                                            document.querySelector(`#${getPost.val().postId} .m_postArticleCover`).src = url;
                                            console.log(`page sub-img success`);
                                        }).catch(err => { console.log(err.message);});
                                    }
                                    else if(getPost.val().postCoverPic === 'url') {
                                        firebase.database().ref(`m_imageUrl/${childSnapshot.key}/${getPost.val().postId}`).once('value').then(url => {
                                            document.querySelector(`#${getPost.val().postId} .m_postArticleCover`).src = url.val().postCoverPicUrl;
                                        }).catch(err => { console.log(err.message); });
                                    }
                                    else {
                                        document.querySelector(`#${getPost.val().postId} .m_postArticleCover`).remove();
                                    }

                                }
                            }).catch(err => { console.log(err, err.message); });
                        }
                    }).catch(err => { console.log(err.message); });
                });
                // if(this.reversePosts.length == userPosts.numChildren() && i == 0) {
                //     for(let name of this.subject) {
                //         if(document.querySelector(`#${name}`).children.length == 0) {
                //             document.querySelector(`#${name}`).remove();
                //         }
                //     }
                // }
            }
        });
    }
}