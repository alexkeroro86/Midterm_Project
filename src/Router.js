import Category from './Category.js';

export default class Router {
    constructor(isHistory = false, root = '/', m_board) {
        this.isHistory = isHistory;
        this.root = root;
        this.routes = [];

        this.m_board = m_board;

        this.on(/category\/(.*)\/(.*)/, (subjectName, postId) => {
            console.log(subjectName, postId);
            this.m_board.root.innerHTML = `
            <div class="m_postArticleWrapper m_container container">
                <div id="m_subjectWelcome" class="row m_container">
                    <img class="img-fluid" width="880px" height="220px">
                    <h1><h1>
                </div>
                <div class="m_postArticle row mt-2 m_container form-control">
                    <div class="row">
                        <img class="m_postArticleCover">
                        <div class="w-100"></div>
                        <h2 class="m_postArticleTitle"></h2>
                    </div>
                    <div class="row m-3">
                        <div class="m_postArticleAnD col-md-3 col-12">
                            <span class="m_postArticleAuthor"></span><br/>
                            <span class="m_postArticleDate" style="color:#fcfcfc;font-size:1rem;position:absolute;bottom:0;right:0;"></span> 
                        </div>
                        
                        <div class="m_postArticlePost col-md-8 col-12">
                        </div>
                    </div>
                    <div class="row justify-content-center"><span class="m_postArticleLike"><i class="far fa-thumbs-up"></i><span class="m_postArticleLikeNum"></span></span></div>
                </div>
                <div id="m_postComment" class="m_container">
                </div>
                <div class="m_newComment form-control mt-3 wavetext"></div>
                <div class="m_postArticleCommentWrapper form-control">
                    <div class="m_postArticleComment mt-2">
                        <h4>Comment</h4>
                        <textarea class="form-control white" rows="5"></textarea>
                    </div>
                    <div class="media text-muted pt-3">
                        <button type="button" class="m_commentBtn btn btn-success ml-auto">Submit</button>
                    </div>
                </div>
            </div>
            `;
            this.m_board.setPost(subjectName, postId);
        }).on(/category\/(.*)/, (subjectName) => {
            this.m_board.root.innerHTML = `
            <div class="m_subject m_container container">
                <div id="m_subjectWelcome" class="row">
                    <img class="img-fluid" width="880px" height="220px">
                    <h1><h1>
                </div>
                <div id="m_subjectPost" class="row rounded">
                    <div class="m_newPost form-control mt-3 wavetext"></div>
                    <div class="m_post form-control">
                        <div class="m_coverPic">
                            <h4>Cover Picture
                                <label id="upload-img-wrapper">
                                    <i class="far fa-image"></i>
                                    <input id="upload-img" style="display:none;" type="file">
                                </label>
                                <label>
                                    <i class="fas fa-link"></i>
                                    <input id="upload-img-url" type="text">
                                </label>
                            </h4>
                            <img id="m_uploadPic" class="form-control">
                        </div>
                        <div class="m_title">
                            <h4>Title</h4>
                            <textarea class="form-control white" rows="1"></textarea>
                        </div>
                        <div class="m_comment mt-2">
                            <h4>Content</h4>
                            <div class="m_editorBar">
                                <button type="button" id="m_toBold"><i class="fas fa-bold"></i></button>
                                <button type="button" id="m_toItalic"><i class="fas fa-italic"></i></button>
                                <div class="color-picker-wrapper" style="position:relative">
                                    <button type="button"><i class="fas fa-paint-brush"></i></button>
                                    <div style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                        <input id="m_toColor" type="color" value="#555555" style="width:100%;height:100%;"></input>
                                    </div>
                                </div>
                                <button type="button" id="m_toLink"><i class="fas fa-link"></i></button>
                                </div>
                                <div class="m_editor form-control white" contenteditable>
                            </div>
                        </div>
                        <div class="media text-muted pt-3">
                            <button type="button" class="m_postBtn btn btn-success ml-auto">Submit</button>
                        </div>
                    </div>
                </div>
                <div id="m_subjectList" class="card-columns mt-2">
                </div>
            </div>
            `;
            this.m_board.setSubject(subjectName);
        }).on(/category/, () => {
            document.querySelector('#m_goBack').style.display = 'block';
            this.m_board.root.innerHTML = `
            <div class="m_category m_container card-columns" style="opacity:0;">
                <div class="m_categorySubject card">
                    <img class="card-img-top" src="images/games.jpg" alt="GAMES image cap">
                    <div class="card-body">
                        <h5 class="card-title">GAMES</h5>
                        <p id="m_latest_games_title" class="card-text">NO TITLE</p>
                        <p class="m_small card-text"><small id="m_latest_games_date" class="text-muted">Last updated &infin; ago</small></p>
                    </div>
                </div>
                <div class="m_categorySubject card">
                    <img class="card-img-top" src="images/sports.jpg" alt="SPORTS image cap">
                    <div class="card-body">
                        <h5 class="card-title">SPORTS</h5>
                        <p id="m_latest_sports_title" class="card-text">NO TITLE</p>
                        <p class="m_small card-text"><small id="m_latest_sports_date" class="text-muted">Last updated &infin; ago</small></p>
                    </div>
                </div>
                <div class="m_categorySubject card">
                    <img class="card-img-top" src="images/news.jpg" alt="NEWS image cap">
                    <div class="card-body">
                        <h5 class="card-title">NEWS</h5>
                        <p id="m_latest_news_title" class="card-text">NO TITLE</p>
                        <p class="m_small card-text"><small id="m_latest_news_date" class="text-muted">Last updated &infin; ago</small></p>
                    </div>
                </div>
                <div class="m_categorySubject card">
                    <img class="card-img-top" src="images/studies.jpg" alt="STUDIES image cap">
                    <div class="card-body">
                        <h5 class="card-title">STUDIES</h5>
                        <p id="m_latest_studies_title" class="card-text">NO TITLE</p>
                        <p class="m_small card-text"><small id="m_latest_studies_date" class="text-muted">Last updated &infin; ago</small></p>
                    </div>
                </div>
                <div class="m_categorySubject card">
                    <img class="card-img-top" src="images/foods.jpg" alt="FOODS image cap">
                    <div class="card-body">
                        <h5 class="card-title">FOODS</h5>
                        <p id="m_latest_foods_title" class="card-text">NO TITLE</p>
                        <p class="m_small card-text"><small id="m_latest_foods_date" class="text-muted">Last updated &infin; ago</small></p>
                    </div>
                </div>
                <div class="m_categorySubject card">
                    <img class="card-img-top" src="images/animals.jpg" alt="ANIMALS image cap">
                    <div class="card-body">
                        <h5 class="card-title">ANIMALS</h5>
                        <p id="m_latest_animals_title" class="card-text">NO TITLE</p>
                        <p class="m_small card-text"><small id="m_latest_animals_date" class="text-muted">Last updated &infin; ago</small></p>
                    </div>
                </div>
                <div class="m_categorySubject card">
                    <img class="card-img-top" src="images/notices.jpg" alt="NOTICES image cap">
                    <div class="card-body">
                        <h5 class="card-title">NOTICES</h5>
                        <p id="m_latest_notices_title" class="card-text">NO TITLE</p>
                        <p class="m_small card-text"><small id="m_latest_notices_date" class="text-muted">Last updated &infin; ago</small></p>
                    </div>
                </div>
            </div>
            `;
            this.m_board.setCategory();
        }).on(/signup/, () => {
            this.m_board.root.innerHTML = `
            <div class="m_alert">
            </div>
            <div class="m_signUpForm" style="opacity:0;">
                <h1 class="h3 mb-3 font-weight-normal font-weight-bold">SIGN UP</h1>
                <div><label for="inputEmail" class="sr-only"></label><label for="inputEmail">Email address</label></div>
                <input type="email" id="inputEmail" class="form-control" placeholder="Enter your email address..." required autofocus>
                <div><label for="inputPassword" class="sr-only"></label><label for="inputPassword">Password</label></div>
                <input type="password" id="inputPassword" class="form-control" placeholder="Enter your password..." required>
                <div><label for="inputUsername" class="sr-only"></label><label for="inputUsername">Username</label></div>
                <input type="text" id="inputUsername" class="form-control" placeholder="Enter your username..." required>
                <button class="btn btn-lg btn-primary btn-block" id="btnSignUp">Sign up</button>
                <button class="btn btn-lg btn-secondary btn-block" id="btnSignUpBack">Back</button>
            </div>
            `;
            this.m_board.setSignUpForm();
        }).on(/signin/, () => {
            this.m_board.root.innerHTML = `
            <div class="m_alert">
            </div>
            <div class="m_signInForm">
                <h1 class="h3 mb-3 font-weight-normal font-weight-bold">SIGN IN</h1>
                <div><label for="inputEmail" class="sr-only"></label><label for="inputEmail">Email address</label></div>
                <input type="email" id="inputEmail" class="form-control" placeholder="Enter your email address..." required autofocus>
                <div><label for="inputPassword" class="sr-only"></label><label for="inputPassword">Password</label></div>
                <input type="password" id="inputPassword" class="form-control" placeholder="Enter your password..." required>
                <button class="btn btn-lg btn-primary btn-block" id="btnSignIn">Sign in</button>
                <button class="btn btn-lg btn-info btn-block" id="btnGoogle">Sign in with Google</button>
                <button class="btn btn-lg btn-secondary btn-block" id="btnSignInBack">Back</button>
            </div>
            `;
            this.m_board.setSignInForm();
        }).on(/profile\/(.*)/, (uid) => {
            firebase.database().ref(`m_user/${uid}`).once('value').then(userProfile => {
                this.m_board.root.innerHTML = `
                <div class="m_alert">
                </div>
                <div class="m_profileForm">
                    <h1 class="h3 mb-3 font-weight-normal font-weight-bold">${userProfile.val().name}\'s Profile</h1>
                    <div><label for="inputName" class="sr-only"></label><label for="inputName">User name</label></div>
                    <input type="text" id="inputName" class="form-control" value="${userProfile.val().name}" required autofocus>
                    <div><label for="inputEmail" class="sr-only"></label><label for="inputEmail">Email address</label></div>
                    <input type="email" id="inputEmail" class="form-control" value="${userProfile.val().email}" required>
                    <button class="btn btn-lg btn-info ml-1 mt-2" id="btnUpdate">Update</button>
                    <button class="btn btn-lg mt-2" id="btnProfileBack">Back</button>
                </div>
                `;
                this.m_board.setProfileForm(userProfile.val());
            }).catch(err => {
                alert(err.message);
                this.navigate('/category');
            });
        }).on(/page\/(.*)/, (uid) => {
            firebase.database().ref(`m_user/${uid}`).once('value').then(userProfile => {
                this.m_board.root.innerHTML = `
                <div class="m_personalPage">
                </div>
                `;
                this.m_board.setPersonalPage(userProfile.val());
            }).catch(err => {
                alert(err.message);
            });
        }).on(/notfound/, () => {
            this.m_board.root.innerHTML = `
                <div class="m_container">
                    <div id="m_notFound">
                        <h1>NOT FOUND</h1>
                    </div>
                    <div style="position: absolute;left: 50%;top: 75%;transform: translate(-50%, -75%);">
                        <button id="m_back2Category" type="button" class="btn btn-danger">Back to Category</button>
                    </div>
                </div>
            `;
            document.querySelector('#m_back2Category').addEventListener('click', () => {
                this.navigate('/category');
            });
            document.querySelector('#m_goBack').style.display = 'none';
        }).on(() => {
            this.navigate('/notfound');
        }).listen();
    }
  
    on(...args) {
        if(typeof args[0] === 'function') {
            this.routes.push({ dir: '', handler: args[0]});
        }
        else {
            this.routes.push({ path: args[0], handler: args[1]});
        }
        return this;
    }
    off(handler) {
        this.routes = this.routes.reduce((res, route) => {
            if(route.handler !== handler) {
                res.push(route);
            }
            return res;
        }, []);
    }
    navigate(path) {
        path = path ? path : '';
        if(this.isHistory) {
            history.pushState(null, null, this.root + this.clearSlashes(path));
        }
        else {
            window.location.href = window.location.href.replace(/#$/, '').replace(new RegExp('#!' + '.*$'), '') + '#!' + path;
        }
        return this;
    }
    getFragment() {
        let fragment = '';
        if(this.isHistory) {
            fragment = this.clearSlashes(decodeURI(location.pathname + location.search));
            fragment = fragment.replace(/\?(.*)$/, '');
            fragment = this.root != '/' ? fragment.replace(this.root, '') : fragment;
        } else {
            var match = window.location.href.match(/#(.*)$/);
            fragment = match ? match[1] : '';
        }
        return this.clearSlashes(fragment);
    }
    clearSlashes(path) {
        return path.toString().replace(/\/$/, '').replace(/^\//, '');
    }
    reset() {
        this.routes = [];
        this.isHistory = false;
        this.root = '/';
        return this;
    }
    check(path) {
        let fragment = path || this.getFragment();
        for(let i = 0; i < this.routes.length; ++i) {
            let match = fragment.match(this.routes[i].path);
            if(match) {
                match.shift();
                this.routes[i].handler.apply({}, match);
                return this;
            }           
        }
        return this;
    }
    listen() {
        var self = this;
        var current = self.getFragment();
        clearInterval(this.interval);
        this.interval = setInterval(() => {
            if(current !== self.getFragment()) {
                current = self.getFragment();
                self.check(current);
            }
        }, 50);
        return this;
    }
  }