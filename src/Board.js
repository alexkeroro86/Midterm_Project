import Component from  './Component.js';
import SignUpForm from './SignUpForm.js';
import SignInForm from './SignInForm.js';
import ProfileForm from './ProfileForm.js';
import Category from './Category.js';
import Subject from './Subject.js';
import Post from './Post.js';
import Page from './Page.js';

import './Board.css';

export default class Board extends Component {
    static getRootClass() {
        return '.m_board';
    }

    constructor(root) {
        super(root);

        this.m_category = new Category(root.querySelector(Category.getRootClass()));
        this.m_category.on('click', this.handleCategorySubjectClick.bind(this));
        
        this.m_origin = root.innerHTML;
        this.m_previous = '';
        this.m_previousValid = true;

        this.m_signUpForm = null;
        this.m_signInForm = null;
        this.m_profileForm = null;
        this.m_personalPage = null;
        this.m_subject = null;
        this.m_post = null;

        this.listRef = firebase.database().ref('m_list');
    }

    reset() {
        this.root.innerHTML = this.m_origin;
    }

    setSignUpForm() {
        this.m_signUpForm = new SignUpForm(this.root.querySelector(SignUpForm.getRootClass()));
        
        this.m_signUpForm.on('signUpBack', () => {
            this.fire('signup', 'back');
        });
        this.m_signUpForm.on('signUpSuccess', () => {
            this.fire('signup', 'success');
        });
    }

    setSignInForm() {
        this.m_signInForm = new SignInForm(this.root.querySelector(SignInForm.getRootClass()));

        this.m_signInForm.on('signInBack', () => {
            this.fire('signin', 'back');
        });
        this.m_signInForm.on('signInSuccess', () => {
            this.fire('signin', 'success');
        });
    }

    setProfileForm(userProfile) {
        this.m_profileForm = new ProfileForm(this.root.querySelector(ProfileForm.getRootClass()), userProfile);

        this.m_profileForm.on('profileBack', () => {
            this.fire('profile', 'back');
        });
    }

    setPersonalPage(userProfileVal) {
        this.m_personalPage = new Page(this.root.querySelector(Page.getRootClass()), userProfileVal);
        this.m_personalPage.on('personalPagePostClick', (firer, subjectName, postId) => {
            this.fire('personalPagePostClickFromBoard', subjectName, postId);
        });
    }

    setCategory() {
        this.m_category = new Category(this.root.querySelector(Category.getRootClass()));
        this.m_category.on('click', this.handleCategorySubjectClick.bind(this));
    }

    setSubject(subjectName) {
        this.m_subject = new Subject(this.root.querySelector(Subject.getRootClass()), subjectName);
        this.m_subject.on('postClick', (firer, subjectName, postId) => {
            this.fire('postClickFromBoard', subjectName, postId);
        });
        this.m_subject.on('subjectNotFound', () => {
            this.fire('subjectNotFoundFromBoard');
        });
    }

    handleCategorySubjectClick(firer, subjectName) {
        this.fire('subject', subjectName);
    }

    setPost(subjectName, postId) {
        this.m_post = new Post(this.root.querySelector(Post.getRootClass()), subjectName, postId);
        this.m_post.on('postNotFound', () => {
            this.fire('postNotFoundFromBoard');
        });
    }
}