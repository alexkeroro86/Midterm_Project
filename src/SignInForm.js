import Component from  './Component.js';

import './SignInForm.css';

export default class SignInForm extends Component {
    static getRootClass() {
        return '.m_signInForm';
    }

    constructor(root) {
        super(root);

        setTimeout(() => {
            root.style.opacity = "1";
        }, 10);

        this.m_alert = document.querySelector('.m_alert');

        this.inputEmail = root.querySelector('#inputEmail');
        this.inputPassword = root.querySelector('#inputPassword');

        this.btnSignIn = root.querySelector('#btnSignIn');
        this.btnGoogle = root.querySelector('#btnGoogle');
        this.btnSignInBack = root.querySelector('#btnSignInBack');

        this.btnSignIn.addEventListener('click', () => {
            let email = inputEmail.value;
            let password = inputPassword.value;

            firebase.auth().signInWithEmailAndPassword(email, password)
                .then(res => {
                    this.create_alert("success", "Welcome to TsingHua Forum");
                    this.fire('signInSuccess');
                })
                .catch(err => {
                    this.create_alert("error", err.message);
                    inputEmail.value = "";
                    inputPassword.value = "";
                });
        });
        this.btnGoogle.addEventListener('click', () => {
            let provider = new firebase.auth.GoogleAuthProvider();

            firebase.auth().signInWithPopup(provider)
                .then(res => {
                    console.log(res.additionalUserInfo.profile);
                    // console.log(firebase.auth().currentUser.password, firebase.auth().currentUser);
                    let user = firebase.auth().currentUser;
                    // firebase.database().ref(`m_user`).once('value').then(snapshot => {
                    //     if(snapshot.hasChild(`${user.uid}`)) {
                    //         console.log('logged');
                    //     }
                    //     else {
                            console.log(user.uid, user.email, res.additionalUserInfo.profile.name);
                            firebase.database().ref(`m_user/${user.uid}`).update({
                                uid: user.uid,
                                email: user.email,
                                password: '',
                                name: res.additionalUserInfo.profile.name
                            });
                        //  }
                        this.create_alert("success", "");
                        setTimeout(this.fire('signInSuccess'), 1000);
                    // }).catch(err => { console.log(err.message); });                        
                })
                .catch(err => {
                    this.create_alert("error", err.message);
                });
        });
        this.btnSignInBack.addEventListener('click', () => {
            this.fire('signInBack');
        });
    }

    reset() {
        // do nothing
    }

    create_alert(type, message) {
        if (type == "success") {
            this.m_alert.innerHTML = `
            <div class='alert alert-success alert-dismissible fade show' role='alert'>
                <strong>Success!</strong>
                <span>${message}</span>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            </div>
            `;
        } else if (type == "error") {
            this.m_alert.innerHTML = `
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                <strong>Error!</strong>
                <span>${message}</span>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            </div>
            `;
        }
    }
}